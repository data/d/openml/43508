# OpenML dataset: Anime-Data

https://www.openml.org/d/43508

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Japanese animation, which is known as anime, has become internationally widespread nowadays. This dataset provides data on anime taken from Anime News Network. 
Content
This dataset consists of 1563 anime data, with the following columns:

Title
Type
number of episodes
etc

Acknowledgements
The dataset was collected from http://www.myanimelist.net
Inspiration
This dataset can be used to build recommendation systems, predict a score, visualize anime similarity, etc.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43508) of an [OpenML dataset](https://www.openml.org/d/43508). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43508/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43508/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43508/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

